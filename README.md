# Fedora Infra Zuul jobs repository

This repository contains jobs for [Fedora Zuul CI](https://fedora.softwarefactory-project.io/) used by multiple Fedora infrastructure projects.

## Adding a new job

To add a new job, please update `.zuul.yaml` and create a new job following the [zuul guidelines](https://softwarefactory-project.io/docs/zuul/reference/job_def.html). Keep in mind that the name of the job needs to be unique on [Fedora Zuul CI](https://fedora.softwarefactory-project.io/).

## List of jobs

Following is the list of currently available jobs in this repository:

* `fi-tox-mypy` - Run static-linker tests with mypy.
* `fi-tox-lint` - Run linter on python code.
* `fi-tox-format` - Run format tests using tox.
* `fi-tox-python37` - Run unit tests for a Python project under cPython version 3.7.
* `fi-tox-python38` - Run unit tests for a Python project under cPython version 3.8.
* `fi-tox-python39` - Run unit tests for a Python project under cPython version 3.9.
* `fi-tox-python310` - Run unit tests for a Python project under cPython version 3.10.
* `fi-tox-docs` - Build docs in tox.
* `fi-tox-bandit` - Run bandit tests on python code.
* `fi-tox-diff-cover` - Run coverage on python code.
* `fi-ansible-lint-diff` - Run ansible-lint on changed playbooks/roles in PR.
* `fi-yamllint-diff` - Run yamllint on changed yaml files in PR.
* `fi-pre-commit` - Run pre-commit hooks on the code.
* `fi-pytest-rawhide` - Run pytest tests on Fedora rawhide.
* `fi-pytest-f36` - Run pytest tests on Fedora 36.
* `fi-pytest-f35` - Run pytest tests on Fedora 35.
* `fi-pytest-f34` - Run pytest tests on Fedora 34.
